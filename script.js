let tabs = document.querySelectorAll('.tabs-title');
const tabsContent = document.querySelector('.tabs-content').children;

for (let i = 0; i < tabs.length; i++){
    tabs[i].addEventListener('click', event =>{
            let tabsCurrent = event.target.parentElement.children;
            for(let j = 0; j < tabsCurrent.length; j++){
                tabsCurrent[j].classList.remove('active');
            } 
            event.target.classList.add('active');
            let contentCurrent = event.target.parentElement.nextElementSibling.children;
            for(let c = 0; c < contentCurrent.length; c++){
                contentCurrent[c].classList.add('content');
            }
            tabsContent[i].classList.remove('content'); 
        })
}

